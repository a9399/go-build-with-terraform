## GO SERVICE WITH TERRAFORM
This project builds and exposes the go service in concern on AWS fargate
It is designed as such that

- the service is concern is using the GO language
- code is linted 
- code is built
- code is checked for vulnerabilities and outdated packages
- then the code is containerized using docker
- the built image is sent to a existing ecr
- then if the pipeline is run from the protected main branch AWS infrastruce will be deployed with Terraform to faciliate a ECS fargete cluster
- container is deployed using a task definition and service to the fargate cluster
- its exposed to internet ( LOADBALANCER_DNS)

# instructions to use the repository

1. make a fork of the git repository / clone it , depends on your access levels
2. create a Merge request for the service

## THIS REPO ASSUMES FOLLOWING

- The company uses gitlab as Open Source code repository and collaborative software development platform for its Devops tasks
- Developers has access to the gitlab repos and has the initial knowledge to setup and pull the repository
- merge requests are being done to the protected main branch and these merge requests are being verified before being deployed to the environment
- developers has read access to the necessary resources in AWS
- there is an existing ECR repo that holds the comapany images ( thats why the terraform part for this is not in the repo)
- its alright as of now to store the aws credentials in the gitlab repository as masked variables

## DECISONS ON INFRASTRUCTURE

following decision were made for using amazon ECS fargate

- since developers has nothing else installed on their macbooks. it has to be a serverless service and end to end automated project
- after the deployment of aws infrastructure with terraform there is no operational hassle. everything should be managed ( decision to use fargate insted of ec2)
- code build, container build and push and deployement should be fully automated


