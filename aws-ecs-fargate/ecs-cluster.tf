resource "aws_ecs_cluster" "demo-ecs-cluster" {
  name = "demo-ecs-cluster"

  tags = {
    Name = "demo-ecs-cluster"
  }
}
