resource "aws_ecs_task_definition" "TD" {
  family                   = "go-server"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.iam-role.arn
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  container_definitions = jsonencode([
    {
      name      = "main-container"
      image     = "058264453092.dkr.ecr.eu-central-1.amazonaws.com/demo-app-ecr:19"
      cpu       = 1024
      memory    = 2048
      essential = true
      portMappings = [
        {
          containerPort = 8080 # change containerport here
          hostPort      = 8080 # change host port here
        }
      ]
    }
  ])
}

data "aws_ecs_task_definition" "TD" {
  task_definition = aws_ecs_task_definition.TD.family
}
